# Basic ADR application

This applications shows the Action-Domain-Responder pattern in
action.

Tasks:

- Take some time to familiarise yourself with the application
then write down a rough outline of how the system works.
- Add a middleware which caches successful GET requests. If a
request has already been cached, the cached version should be
returned.
-  The 'language' entry is currently a free-text input but only
a limited number of options are available. Can you modify the
view to show a list of available options? Bonus points if the
options in the view update when you update the options available
to the domain. Use any means that you feel are appropriate for
this task.
- Add a unit test for
`\App\Middleware\LanguageToLowerCaseMiddleware` and for your
new middleware.
